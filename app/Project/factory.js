'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp.project.factory',[])
.factory('userFactory',function($http, $sessionStorage) {
var factory = {};

factory.addUser = function(newUser,callback) {
$http.post('http://localhost:3000/users',newUser).success(function(res) {
$sessionStorage.user = res;
callback(res);
})
}
factory.logout = function() {
delete $sessionStorage.user;
}
factory.user = function() {
return $sessionStorage.user;
}
factory.showUser = function(id,callback) {
$http.get('http://localhost:3000/users/'+id).success(function(res) {
callback(res);
})
}
return factory;
})

.factory('socket', ['$rootScope', function($rootScope) {
    
 var socket = io.connect();
 return {
on: function(eventName, callback){
 socket.on(eventName, callback);
			    },
 emit: function(eventName, data) {
socket.emit(eventName, data);
}
};
}])

.factory('categoryFactory',function($http) {
    
var factory = {};
var categories = [];
factory.getCategories = function(callback) {
$http.get('http://localhost:3000/categories').success(function(res) {
categories = res;
callback(categories);
})
}
return factory;
})

.factory('topicFactory',function($http,$sessionStorage) {
var factory = {};
var topics = [];
var currentTopicId = "";			

factory.getTopics = function(callback) {
$http.get('http://localhost:3000/topics').success(function(res) {
topics = res;
$sessionStorage.topics = res;
callback(topics);
})
}

factory.addTopic = function(newTopic,callback) {
$http.post('http://localhost:3000/topics',newTopic).success(function(res) {
topics.push(res);
callback(res);
})
}

factory.showTopic = function(index,callback) {
	
var id = $sessionStorage.topics[index]._id;
				currentTopicId = id;

$http.get('http://localhost:3000/topics/'+id).success(function(res) {
$sessionStorage.topic = res;
callback(res);
})
}


factory.topic = function() {
				return $sessionStorage.topic;
			}

factory.addPost = function(newPost,callback) {
var userId = $sessionStorage.user._id;
$http.post('http://localhost:3000/topics/'+currentTopicId,{text:newPost.text,user:userId}).success(function(res) {
callback(res);
})
}

factory.updatePost = function(postId,action,callback) {	$http.patch('http://localhost:3000/posts/'+postId,{vote:action}).success(function(res) {
callback(res);

})
}

factory.addComment = function(post,callback) {
var userId = $sessionStorage.user._id;
var postId = post._id;
$http.post('http://localhost:3000/posts/'+postId,{user:userId,comment:post.newComment}).success(function(res) {				
callback(res);
})
}
return factory;
})