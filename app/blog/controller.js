'use strict';
// Declare app level module which depends on views, and components
angular.module('myApp.blog.controller',['Alertify'])

.controller('blogController',function($scope,blogfactory,socket,$sessionStorage) {

			$scope.$storage = $sessionStorage;

			$scope.blogs = blogfactory.getblog(function(blogs) {
				$scope.blogs = blogs;
			})

			
		})

.controller('blogPostController',function($rootScope,$scope,blogfactory,$routeParams,$log,userFactory,socket,$location,$sessionStorage) {
			$scope.user = $sessionStorage.user;


			$scope.blog = blogfactory.showblog($routeParams.index,function(res) {
                console.log($routeParams.index);
				$scope.blog = res;
			})
            
            
            $scope.upVote = function() {
                
            blogfactory.updateblog(function(res) {
				$scope.blog = res;
			})
            }
            
          /*  $scope.addComment = function(index,newComment) {

				blogfactory.addComment(index,newComment,function(res) {
                socket.emit('BlogComment');

					//$scope.blog.comment.push(data);
                    console.log(res);
				})

			}*/
})

.controller('blogCommentController',function($rootScope,$scope,blogfactory,$routeParams,$log,userFactory,socket,$location,$sessionStorage) {
			$scope.user = userFactory.user;

                        
            
            $scope.upVote = function(index,newComment) {

				blogfactory.addComment(index,newComment,function(res) {
               // socket.emit('BlogComment');

					//$scope.blog.comment.push(data);
                    console.log(res);
				})

			}
})

.controller('addBlogController',function($rootScope,$scope,blogfactory,$routeParams,$log,userFactory,socket,$location,$sessionStorage,Alertify,$window) {


			$scope.addBlog = function(title,Description,image_id) {
                console.log(image_id);

				blogfactory.addblog(title,Description,image_id,function(res) {
                    console.log(title);
                    console.log(Description);
                    console.log(image_id);
					//$scope.blog.comment.push(data);
                    Alertify.alert("Article ajouté avec success.", function(){ 
    Alertify.message('OK');
  });
                        $window.location.href ="#/blog";
                    console.log(res);
                    
                                       socket.emit('newBlog', res);

                    
				})

			}
            

			
		})
