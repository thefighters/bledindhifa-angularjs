'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.forum.controller',
    'myApp.forum.factory',
    'myApp.blog.controller',
    'myApp.blog.factory',
    'myApp.stat.controller',
    'myApp.stat.factory',
	'myApp.events.controller',
    'myApp.events.factory',
	'project.controller',
	'project.factory',
    'myApp.competitions.controller',
    'myApp.concours.factory',
    'myApp.photo.controller',
    'myApp.photo.factory',
    'ngStorage'  
    
        
    ])


  .config(['$routeProvider', function($routeProvider) {
        $routeProvider
       .when('/donate',{
templateUrl:'association/association.html'
			})
		.when('/blog',{
templateUrl:'blog/blog3.html'
			})
            .when('/blog/rss',{
templateUrl:'blog/blog2.html'
			})
            .when('/blog/new',{
templateUrl:'blog/addBlog.html'
			})
            .when('/blog/:index',{
templateUrl:'blog/post.html'
			})
            .when('/',{
templateUrl:'Forum/user.html'
			})
           
			.when('/dashboard', {
templateUrl:'Forum/Forum.html'
			})
			.when('/topic/:index', {
templateUrl:'Forum/topic.html'		
			})
			.when('/users/:id', {
templateUrl:'Forum/profile.html'}) .when('/stat', {
templateUrl:'stat/stats.html'}) 

            .when('/events', {
templateUrl:'events/events.html'}) 
        
            .when('/events/:id', {
templateUrl:'events/info.html'       
        })
            .when('/participate/:id', {
templateUrl:'events/info.html'       
        })
		
		//rim
		
			.when('/ajout',{
            templateUrl:'Project/create.html',
			controller:'AddProjectCtrl'
           
            })
		
		.when('/projects',{
                templateUrl:'Project/blog-masonry.html',
						controller:'ShowProjectCtrl'

           
            })
		
		.when('/donate/:id/:price',{
		
		  templateUrl:'Project/donate.html',
			controller:'donateProjectCtrl'
		})
		
		
			.when('/sponsor/:id',{
		
		  templateUrl:'Project/sponsor.html',
			controller:'sponsorProjectCtrl'
		})
		
		
		 .when('/projects/:id',{
		
		  templateUrl:'Project/blog-masonry.html',
		  controller:'ProjectByCategCtrl'
		})
		
		
		
		.when('/project/:id',{
                templateUrl:'Project/open-project.html',
			 controller:'ShowProjectDetailsCtrl'

           
            })
		
		
		
		
		
		//fin rim
        
        .when('/concours', {
templateUrl:'concours/concours.html'}) 
		
		 .when('/concours/:id', {
templateUrl:'concours/info.html'  
        })
        
        	 .when('/concours/upload/:id', {
templateUrl:'concours/upload.html'  
        })
        
        .when('/image/:id', {
templateUrl:'photo/info.html'  
        })
            
        .when('/erreur', {
templateUrl:'404.html'  
        })
        
    .otherwise({redirectTo: '/erreur'});
    }])

;