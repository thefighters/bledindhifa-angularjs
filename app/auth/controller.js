    angular.module("myApp.register.controller",[])
        .controller("RegisterCtrl", function ($scope, $location, $rootScope, UserService, $sessionStorage, $window, $http)
    {
        $scope.fbLogin = function () {
        FB.login(function (response) {

                console.log(response);

            if (response.authResponse) {
                console.log(response.authResponse);
                getUserInfo();
            } else {
            }
        }, {scope: 'email,user_photos,user_videos'});

        function getUserInfo() {
            // get basic info
            FB.api('/me', function (response) {
                // get profile picture
         console.log(response);

                FB.api('/me/picture?type=normal', function (picResponse) {
                    response.imageUrl = picResponse.data.url;
                    //Save user
                    
         var user ={
                    username :response.name,
                    socialAuth:'facebook',
				    image_link : picResponse.data.url};
                    $http.post("http://bledi--ndhifa.herokuapp.com/auth/register", user);
                    socket.emit('newUser', user.username);
                                    console.log(user);


                   $sessionStorage.user = user;
                   $location.path("/");
                    
                });
            });
        }}
        $scope.register = register;

        function register(user, image_id)
        {
            user.image_id = image_id;
            if(user.password != user.password2 || !user.password || !user.password2)
            {
                $scope.error = "Your passwords don't match";
            }
            else
            {
                UserService
                    .register(user)
                    .then(
                        function(response) {
                    var user = response.data;
           
                            console.log(response.data);
console.log("ameni"+response);
                           if( response.data.message == "user already exists"){
                            $window.alert('Nom d\'utilisateur existant');
                            $window.location.reload();

                            }
                            else if(response.data.user.username != null) {
                                $rootScope.currentUser = user;
                                $window.alert('Votre compte a été créer, veuillez vous identifer');
                                $location.path("/login");
                            }
                            
                            
           
                            
                        },
                        function(err) {
                            $scope.error = err;
                            console.log(err)
                        }
                    );
            }
            console.log("hhhhhh"+user);
             socket.emit('newUser', user);
        }
    }
)
    .controller("RegisterAssociationCtrl", function ($scope, $location, $rootScope, UserService, $sessionStorage, $window)
    {
        $scope.register = register;

        function register(user, image_id)
        {
            user.roles="association";
            user.image_id = image_id;
            if(user.password != user.password2 || !user.password || !user.password2)
            {
                $scope.error = "Your passwords don't match";
            }
            else
            {
                UserService
                    .register(user)
                    .then(
                        function(response) {
                            var user = response.data;
                            console.log(response.data);
                           if( response.data.message == "user already exists"){
                            $window.alert('Nom d\'utilisateur existant');
                            $window.location.reload();

                            }
                            else if(response.data.user.username != null) {
                                $rootScope.currentUser = user;
                                $window.alert('Votre compte a été créer, veuillez vous identifer');
                                $window.location.path("/login");
                            }
                        },
                        function(err) {
                            $scope.error = err;
                            console.log(err)
                        }
                    );
            }
        }
    }
               )
    .controller("ProfileCtrl", function ($scope, $location, $rootScope, UserService, $sessionStorage, $window,Alertify)
    {
        $scope.user=$sessionStorage.user;
         $scope.logout = function() {
                
            $sessionStorage.user={};
                            Alertify.alert("Déconnection...", function(){
    Alertify.message('OK');
  });
                $location.path("/");
			}
            }
            

)

